/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/build/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

$(document).ready(function () {

    // --------------------------- multidimensional menu ---------------------------

    $(".dropdown-menu > li > a.trigger").on("click", function (e) {
        console.log(5);
        var current = $(this).next();
        var grandparent = $(this).parent().parent();
        if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret')) $(this).toggleClass('right-caret left-caret');
        grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
        grandparent.find(".sub-menu:visible").not(current).hide();
        current.toggle();
        e.stopPropagation();
    });
    $(".dropdown-menu > li > a:not(.trigger)").on("click", function () {
        var root = $(this).closest('.dropdown');
        root.find('.left-caret').toggleClass('right-caret left-caret');
        root.find('.sub-menu:visible').hide();
    });

    // --------------------------- Slider ---------------------------
    $('.single-item').slick({
        autoplay: true,
        autoplaySpeed: 3000
    });
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgY2Y3NmYzNDBiMTRkYzIyOWNhYTYiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsIm9uIiwiZSIsImNvbnNvbGUiLCJsb2ciLCJjdXJyZW50IiwibmV4dCIsImdyYW5kcGFyZW50IiwicGFyZW50IiwiaGFzQ2xhc3MiLCJ0b2dnbGVDbGFzcyIsImZpbmQiLCJub3QiLCJoaWRlIiwidG9nZ2xlIiwic3RvcFByb3BhZ2F0aW9uIiwicm9vdCIsImNsb3Nlc3QiLCJzbGljayIsImF1dG9wbGF5IiwiYXV0b3BsYXlTcGVlZCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDN0RBQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVTs7QUFFeEI7O0FBRUFGLE1BQUUsaUNBQUYsRUFBcUNHLEVBQXJDLENBQXdDLE9BQXhDLEVBQWdELFVBQVNDLENBQVQsRUFBVztBQUN2REMsZ0JBQVFDLEdBQVIsQ0FBWSxDQUFaO0FBQ0EsWUFBSUMsVUFBUVAsRUFBRSxJQUFGLEVBQVFRLElBQVIsRUFBWjtBQUNBLFlBQUlDLGNBQVlULEVBQUUsSUFBRixFQUFRVSxNQUFSLEdBQWlCQSxNQUFqQixFQUFoQjtBQUNBLFlBQUdWLEVBQUUsSUFBRixFQUFRVyxRQUFSLENBQWlCLFlBQWpCLEtBQWdDWCxFQUFFLElBQUYsRUFBUVcsUUFBUixDQUFpQixhQUFqQixDQUFuQyxFQUNJWCxFQUFFLElBQUYsRUFBUVksV0FBUixDQUFvQix3QkFBcEI7QUFDSkgsb0JBQVlJLElBQVosQ0FBaUIsYUFBakIsRUFBZ0NDLEdBQWhDLENBQW9DLElBQXBDLEVBQTBDRixXQUExQyxDQUFzRCx3QkFBdEQ7QUFDQUgsb0JBQVlJLElBQVosQ0FBaUIsbUJBQWpCLEVBQXNDQyxHQUF0QyxDQUEwQ1AsT0FBMUMsRUFBbURRLElBQW5EO0FBQ0FSLGdCQUFRUyxNQUFSO0FBQ0FaLFVBQUVhLGVBQUY7QUFDSCxLQVZEO0FBV0FqQixNQUFFLHVDQUFGLEVBQTJDRyxFQUEzQyxDQUE4QyxPQUE5QyxFQUFzRCxZQUFVO0FBQzVELFlBQUllLE9BQUtsQixFQUFFLElBQUYsRUFBUW1CLE9BQVIsQ0FBZ0IsV0FBaEIsQ0FBVDtBQUNBRCxhQUFLTCxJQUFMLENBQVUsYUFBVixFQUF5QkQsV0FBekIsQ0FBcUMsd0JBQXJDO0FBQ0FNLGFBQUtMLElBQUwsQ0FBVSxtQkFBVixFQUErQkUsSUFBL0I7QUFDSCxLQUpEOztBQU1BO0FBQ0FmLE1BQUUsY0FBRixFQUFrQm9CLEtBQWxCLENBQXdCO0FBQ3BCQyxrQkFBVSxJQURVO0FBRXBCQyx1QkFBZTtBQUZLLEtBQXhCO0FBSUgsQ0ExQkQsRSIsImZpbGUiOiJqcy9hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvYnVpbGQvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2Fzc2V0cy9qcy9hcHAuanNcIik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgY2Y3NmYzNDBiMTRkYzIyOWNhYTYiLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSBtdWx0aWRpbWVuc2lvbmFsIG1lbnUgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgJChcIi5kcm9wZG93bi1tZW51ID4gbGkgPiBhLnRyaWdnZXJcIikub24oXCJjbGlja1wiLGZ1bmN0aW9uKGUpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKDUpO1xyXG4gICAgICAgIHZhciBjdXJyZW50PSQodGhpcykubmV4dCgpO1xyXG4gICAgICAgIHZhciBncmFuZHBhcmVudD0kKHRoaXMpLnBhcmVudCgpLnBhcmVudCgpO1xyXG4gICAgICAgIGlmKCQodGhpcykuaGFzQ2xhc3MoJ2xlZnQtY2FyZXQnKXx8JCh0aGlzKS5oYXNDbGFzcygncmlnaHQtY2FyZXQnKSlcclxuICAgICAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygncmlnaHQtY2FyZXQgbGVmdC1jYXJldCcpO1xyXG4gICAgICAgIGdyYW5kcGFyZW50LmZpbmQoJy5sZWZ0LWNhcmV0Jykubm90KHRoaXMpLnRvZ2dsZUNsYXNzKCdyaWdodC1jYXJldCBsZWZ0LWNhcmV0Jyk7XHJcbiAgICAgICAgZ3JhbmRwYXJlbnQuZmluZChcIi5zdWItbWVudTp2aXNpYmxlXCIpLm5vdChjdXJyZW50KS5oaWRlKCk7XHJcbiAgICAgICAgY3VycmVudC50b2dnbGUoKTtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgfSk7XHJcbiAgICAkKFwiLmRyb3Bkb3duLW1lbnUgPiBsaSA+IGE6bm90KC50cmlnZ2VyKVwiKS5vbihcImNsaWNrXCIsZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgcm9vdD0kKHRoaXMpLmNsb3Nlc3QoJy5kcm9wZG93bicpO1xyXG4gICAgICAgIHJvb3QuZmluZCgnLmxlZnQtY2FyZXQnKS50b2dnbGVDbGFzcygncmlnaHQtY2FyZXQgbGVmdC1jYXJldCcpO1xyXG4gICAgICAgIHJvb3QuZmluZCgnLnN1Yi1tZW51OnZpc2libGUnKS5oaWRlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gU2xpZGVyIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgJCgnLnNpbmdsZS1pdGVtJykuc2xpY2soe1xyXG4gICAgICAgIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgICAgIGF1dG9wbGF5U3BlZWQ6IDMwMDBcclxuICAgIH0pO1xyXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9hc3NldHMvanMvYXBwLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==
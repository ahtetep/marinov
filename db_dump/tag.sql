﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.78.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 02.03.2018 12:01:33
-- Версия сервера: 5.5.5-10.1.30-MariaDB
-- Версия клиента: 4.1
--


SET NAMES 'utf8';

INSERT INTO db_marinov.tag(id, name) VALUES
(1, 'Politics');
INSERT INTO db_marinov.tag(id, name) VALUES
(2, 'World');
INSERT INTO db_marinov.tag(id, name) VALUES
(3, 'Ukraine');
INSERT INTO db_marinov.tag(id, name) VALUES
(4, 'Sport');
INSERT INTO db_marinov.tag(id, name) VALUES
(5, 'Tech');
INSERT INTO db_marinov.tag(id, name) VALUES
(6, 'Weather');
INSERT INTO db_marinov.tag(id, name) VALUES
(7, 'Shop');
INSERT INTO db_marinov.tag(id, name) VALUES
(8, 'Earth');
INSERT INTO db_marinov.tag(id, name) VALUES
(9, 'Travel');
INSERT INTO db_marinov.tag(id, name) VALUES
(10, 'Capital');
INSERT INTO db_marinov.tag(id, name) VALUES
(11, 'Culture');
INSERT INTO db_marinov.tag(id, name) VALUES
(12, 'Autos');
INSERT INTO db_marinov.tag(id, name) VALUES
(13, 'Future');
INSERT INTO db_marinov.tag(id, name) VALUES
(14, 'TV');
INSERT INTO db_marinov.tag(id, name) VALUES
(15, 'Radio');
INSERT INTO db_marinov.tag(id, name) VALUES
(16, 'Food');
INSERT INTO db_marinov.tag(id, name) VALUES
(17, 'Music');
INSERT INTO db_marinov.tag(id, name) VALUES
(18, 'Arts');
INSERT INTO db_marinov.tag(id, name) VALUES
(19, 'Design');
INSERT INTO db_marinov.tag(id, name) VALUES
(20, 'Taster');
INSERT INTO db_marinov.tag(id, name) VALUES
(21, 'Nature');
INSERT INTO db_marinov.tag(id, name) VALUES
(22, 'Space');
﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.78.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 02.03.2018 12:01:34
-- Версия сервера: 5.5.5-10.1.30-MariaDB
-- Версия клиента: 4.1
--


SET NAMES 'utf8';

INSERT INTO db_marinov.user(id, email, password, created_at, roles) VALUES
(1, 'admin@admin.com', '$2a$12$TlS4rq2/BP44lIig6f/yLOzKG9xMk42rtE/3.CrIG83dmRCfb7HCq', '2018-02-09 00:00:00', 'ROLE_ADMIN,ROLE_USER');
INSERT INTO db_marinov.user(id, email, password, created_at, roles) VALUES
(2, 'user@user.com', '$2a$12$g4nSe1S6O7EsQmH65btyBOkpB6F61r8ktjMLJdW3O3V1P/BamqzUO', '2018-02-10 00:00:00', 'ROLE_USER');
INSERT INTO db_marinov.user(id, email, password, created_at, roles) VALUES
(4, 'sg@dfgfd.rfrg', 'sfgsg', '2018-02-28 19:16:46', 'ROLE_USER');
INSERT INTO db_marinov.user(id, email, password, created_at, roles) VALUES
(5, 'q@q.com', '$2y$12$mGf1qDlwB3kMkpP65KOlhumDoPAZYuCu53utybPDkq2fFtG5TdBzO', '2018-02-28 19:38:49', 'ROLE_USER');
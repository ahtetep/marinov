<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="comment", type="text")
     * @Assert\NotBlank()
     */
    private $comment;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="comments")
     * @Assert\NotBlank()
     */
    private $post;

    /**
     * @ORM\Column(name="author", type="text")
     * @Assert\NotBlank()
     */
    private $author;

    /**
     * @ORM\Column(name="thumbs_up", type="integer")
     */
    private $thumbsUp;

    /**
     * @ORM\Column(name="thumbs_down", type="integer")
     */
    private $thumbsDown;

    /**
     * @ORM\Column(name="approved", type="integer", nullable=true)
     */
    private $approved;

    /**
     * Feedback constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getThumbsUp()
    {
        return $this->thumbsUp;
    }

    /**
     * @param mixed $thumbsUp
     */
    public function setThumbsUp($thumbsUp)
    {
        $this->thumbsUp = $thumbsUp;
    }

    /**
     * @return mixed
     */
    public function getThumbsDown()
    {
        return $this->thumbsDown;
    }

    /**
     * @param mixed $thumbsDown
     */
    public function setThumbsDown($thumbsDown)
    {
        $this->thumbsDown = $thumbsDown;
    }

    /**
     * @return mixed
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * @param mixed $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

}

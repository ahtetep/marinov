<?php

namespace App\Repository;

use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findAllTagInPost($id)
    {
        return $this->createQueryBuilder('t')
            ->leftJoin('t.posts', 'p')
            ->where('p = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getAllTagQuery()
    {
        return $this->createQueryBuilder('tag')
            ->orderBy('tag.id', 'ASC')
            ->getQuery();
    }

    /**
     * @return array
     */
    public function findAllTagAdminTable()
    {
        return $this->findBy([], ['id'=> 'DESC']);
    }
}

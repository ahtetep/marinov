<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoryRepository extends ServiceEntityRepository
{
    const CATEGORY_ANALYTICS = 2;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @return \Doctrine\ORM\Query
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllCategoryQuery()
    {
        $connection = $this->_em->getConnection();
        $stmt = $connection->prepare('
            SELECT 
              category.id as category_id,
              category.name,
              post.title,
              post.post_id
            FROM category
            LEFT JOIN 
                (SELECT 
                  post.category_id,
                  substring_index(GROUP_CONCAT(post.title SEPARATOR "|"), "|", 5) as title,
                  substring_index(GROUP_CONCAT(post.id SEPARATOR "|"), "|", 5) as post_id
                FROM post
                WHERE post.category_id = category_id
                GROUP BY post.category_id
                ORDER BY post.created_at DESC
            ) post ON post.category_id = category.id
        ');
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @return array
     */
    public function findAllCategoryAdminTable()
    {
        return $this->findBy([], ['id' => 'DESC']);
    }
}

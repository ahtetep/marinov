<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CommentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findAllCommentsByPost($id)
    {
        return $this->createQueryBuilder('c')
            ->where('c.post = :id')
            ->setParameter('id', $id)
            ->orderBy('c.thumbsUp', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findAllConfirmedComments($id)
    {
        return $this->createQueryBuilder('c')
            ->where('c.post = :id')
            ->andWhere('c.approved = :approved')
            ->setParameter('id', $id)
            ->setParameter('approved', true)
            ->orderBy('c.thumbsUp', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function findAllCommentAdminTable()
    {
        return $this->findBy([], ['id' => 'DESC']);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findAllCommentsForConfirmation()
    {
        $connection = $this->_em->getConnection();
        $stmt = $connection->prepare('
        select 
        	com.*,
            cat.id as category_id,
            cat.name as category_name
        from comment com
        	inner join post p ON p.id = com.post_id
        	inner join category cat ON cat.id = p.category_id
        where com.approved is null and cat.id = 1
        ');
        $stmt->execute();

        return $stmt->fetchAll();
    }
}

<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PostRepository extends ServiceEntityRepository
{
    const SLIDER_MAX_COUNT = 6;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findAllPost()
    {
        $qb = $this->createQueryBuilder('post');

        return $qb
            ->addOrderBy($qb->expr()->desc('post.createdAt'))
            ->getQuery()
        ;
    }

    /**
     * @return array
     */
    public function findAllPostAdminTable()
    {
        return $this->findBy([], ['id'=> 'DESC']);
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function findPostByCategory(Category $category)
    {
        return $this->createQueryBuilder('post')
            ->where('post.category = :category')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $id
     * @return \Doctrine\ORM\Query
     */
    public function findAllPostsInCategoryQuery($id)
    {
        return $this->createQueryBuilder('post')
            ->where('post.category = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findAllPostsInTagQuery($id)
    {
        return $this->createQueryBuilder('post')
            ->leftJoin('post.tags', 't')
            ->where('t = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return mixed
     */
    public function findLatestPublications()
    {
        $count = PostRepository::SLIDER_MAX_COUNT;

        return $this->createQueryBuilder('post')
            ->orderBy('post.createdAt', 'DESC')
            ->setMaxResults($count)
            ->getQuery()
            ->getResult()
            ;
    }
}

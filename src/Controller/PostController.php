<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->findAllPost();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('post/index.html.twig', [
            'pagination' => $pagination
        ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function showAction(Request $request, $id)
    {
        $user = $this->getUser();

        $post = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->find($id);

        $tags = $this
            ->getDoctrine()
            ->getRepository('App:Tag')
            ->findAllTagInPost($id);

        if (!$post) {
            throw $this->createNotFoundException('Post not found');
        }

        if (1 === $post->getCategory()->getId()) {
            $comments = $this
                ->getDoctrine()
                ->getRepository('App:Comment')
                ->findAllConfirmedComments($id);
        } else {
            $comments = $this
                ->getDoctrine()
                ->getRepository('App:Comment')
                ->findAllCommentsByPost($id);
        }

        $comment = new Comment();
        if ($user) {
            $comment->setAuthor($user->getEmail());
        } else {
            $comment->setAuthor('anonymous');
        }
        $comment->setPost($post);
        $comment->setThumbsUp(0);
        $comment->setThumbsDown(0);

        $form = $this->createForm(CommentType::class, $comment);
        $form->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success'
            ]
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData();
            $comment = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('post_page', ['id' => $id]);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $comments,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('post/show.html.twig', [
            'post' => $post,
            'pagination' => $pagination,
            'form' => $form->createView(),
            'tags' => $tags,
            'user' => $user,
        ]);
    }
}
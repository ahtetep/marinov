<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     */
    public function likeAction(Request $request, $id)
    {
        $comment = $this
            ->getDoctrine()
            ->getRepository('App:Comment')
            ->find($id);

        if (!$comment) {
            throw $this->createNotFoundException('Comment not found');
        }
        $em = $this->getDoctrine()->getManager();

        $thumbsUp = $comment->getThumbsUp();
        $comment->setThumbsUp($thumbsUp+1);

        $em->persist($comment);
        $em->flush();
        $postId = $comment->getPost()->getId();
        return $this->redirectToRoute('post_page', ['id' => $postId]);
        }

    /**
     * @param Request $request
     * @param $id
     */
    public function disLikeAction(Request $request, $id)
    {
        $comment = $this
            ->getDoctrine()
            ->getRepository('App:Comment')
            ->find($id);

        if (!$comment) {
            throw $this->createNotFoundException('Comment not found');
        }
        $em = $this->getDoctrine()->getManager();
        $thumbsUp = $comment->getThumbsDown();
        $comment->setThumbsDown($thumbsUp+1);

        $em->persist($comment);
        $em->flush();

        $postId = $comment->getPost()->getId();
        return $this->redirectToRoute('post_page', ['id' => $postId]);

    }
}
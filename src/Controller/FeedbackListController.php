<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FeedbackListController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $feedbacks = $this
            ->getDoctrine()
            ->getRepository('App:Feedback')
            ->findAll()
        ;

        return $this->render('feedback-list/index.html.twig', [
            'feedbacks' => $feedbacks,
        ]);
    }

}
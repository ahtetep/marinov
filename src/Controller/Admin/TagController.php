<?php

namespace App\Controller\Admin;

use App\Entity\Tag;
use App\Form\TagType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagController extends Controller
{

    /**
     * @return Response
     */
    public function indexAction()
    {
        $tags = $this
            ->getDoctrine()
            ->getRepository('App:Tag')
            ->findAllTagAdminTable()
        ;

        return $this->render('admin/tag/index.html.twig', [
            'tags' => $tags,
        ]);
    }

    public function createAction(Request $request)
    {
        $tag = new Tag();

        $form = $this->createForm(TagType::class, $tag);
        $form->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success',
            ]
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();

            return $this->redirectToRoute('admin_tag_list');
        };

        return $this->render('admin/tag/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, $id )
    {
        $tag = $this
            ->getDoctrine()
            ->getRepository('App:Tag')
            ->find($id)
        ;

        if (!$tag) {
            throw $this->createNotFoundException('Tag not found');
        }

        $form = $this->createForm(TagType::class, $tag);
        $form->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success'
            ]
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();

            $this->addFlash('success', 'Saved');

            return $this->redirectToRoute('admin_tag_edit', ['id' => $id]);
        }

        return $this->render('admin/tag/edit.html.twig', [
            'tag' => $tag,
            'form' => $form->createView()
        ]);
    }
}

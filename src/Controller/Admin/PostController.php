<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\CreatedPostType;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $posts = $this
                ->getDoctrine()
                ->getRepository('App:Post')
                ->findAllPostAdminTable()
        ;

        return $this->render('admin/post/index.html.twig', [
            'posts' => $posts
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $post = new Post();

        $form = $this->createForm(CreatedPostType::class, $post);
        $form->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success',
            ]
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $post->getImagesName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            $file->move(
                $this->getParameter('path_img'), $fileName
            );

            $post->setImagesName($fileName);

            $em = $this->getDoctrine()->getManager();

            $newTag = $form->get('new_tag')->getData();
            if ($newTag) {
                $post->addTag($newTag);

                $em->persist($newTag);
            }
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('admin_posts_list');
        };

        return $this->render('admin/post/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, $id )
    {
        $post = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->find($id)
        ;

        if (!$post) {
            throw $this->createNotFoundException('Post not found');
        }

        $form = $this->createForm(PostType::class, $post);
        $form->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success'
            ]
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $newTag = $form->get('new_tag')->getData();
            if ($newTag) {
                $post->addTag($newTag);

                $em->persist($newTag);
            }

            $em->persist($post);
            $em->flush();

            $this->addFlash('success', 'Saved');

            return $this->redirectToRoute('admin_post_edit', ['id' => $id]);
        }

        return $this->render('admin/post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $doctrine = $this->get('doctrine');
        $post = $doctrine->getRepository('App:Post')->find($request->get('id'));
        if (!$post) {
            $this->addFlash('fail', 'Post not found');
            return $this->redirectToRoute('admin_posts_list');
        }
        $doctrine->getManager()->remove($post);
        $doctrine->getManager()->flush();
        $this->addFlash('success', 'Post deleted');
        return $this->redirectToRoute('admin_posts_list');
    }
}
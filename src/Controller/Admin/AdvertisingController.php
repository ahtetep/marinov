<?php

namespace App\Controller\Admin;

use App\Entity\Advertising;
use App\Form\AdvertisingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class AdvertisingController extends Controller
{

    public function createAction(Request $request)
    {
        $advertising = new Advertising();

        $form = $this->createForm(AdvertisingType::class, $advertising);
        $form->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success',
            ]
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $advertising->getImagesName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            $file->move(
                $this->getParameter('path_img'), $fileName
            );

            $advertising->setImagesName($fileName);

            $em = $this->getDoctrine()->getManager();

            $em->persist($advertising);
            $em->flush();

            return $this->redirectToRoute('admin_posts_list');
        };

        return $this->render('admin/post/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
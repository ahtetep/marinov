<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\CommentType;
use App\Form\CreatedPostType;
use App\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

class ComentController extends Controller
{
    /**
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function indexAction()
    {
        $comments = $this
                ->getDoctrine()
                ->getRepository('App:Comment')
                ->findAllCommentAdminTable()
        ;

        $commentsForConfirmation = $this
            ->getDoctrine()
            ->getRepository('App:Comment')
            ->findAllCommentsForConfirmation()
        ;

        $countCommentsForConfirmation = count($commentsForConfirmation);
        return $this->render('admin/comment/index.html.twig', [
            'comments' => $comments,
            'commentsForConfirmation' => $commentsForConfirmation,
            'countCommentsForConfirmation' => $countCommentsForConfirmation,
        ]);
    }

    public function editAction(Request $request, $id)
    {
        $comment = $this
            ->getDoctrine()
            ->getRepository('App:Comment')
            ->find($id)
        ;

        if (!$comment) {
            throw $this->createNotFoundException('Comment not found');
        }

        $form = $this->createForm(CommentType::class, $comment);
        $form->add('confirm', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success',
            ]
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $comment->setApproved(1);

            $em->persist($comment);
            $em->flush();

            $this->addFlash('success', 'Saved');

            return $this->redirectToRoute('admin_comment_list');
        }

        return $this->render('admin/comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView()
        ]);
    }

    public function deleteAction(Request $request, $id)
    {
        $doctrine = $this->get('doctrine');
        $comment = $doctrine->getRepository('App:Comment')->find($request->get('id'));
        if (!$comment) {
            $this->addFlash('fail', 'Comment not found');
            return $this->redirectToRoute('admin_comment_list');
        }
        $doctrine->getManager()->remove($comment);
        $doctrine->getManager()->flush();
        $this->addFlash('success', 'Comment deleted');
        return $this->redirectToRoute('admin_comment_list');
    }
}
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function indexAction(Request $request)
    {
        $query = $this
            ->getDoctrine()
            ->getRepository('App:Category')
            ->getAllCategoryQuery();

        $posts = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->findLatestPublications();

        foreach ($query as &$category) {
            $category['title'] = explode("|", $category['title']);
            $category['post_id'] = explode("|", $category['post_id']);

            $count = count($category['title']);
            for ($i = 0; $i < $count; $i++) {
                $category['post'][$i]['title'] = $category['title'][$i];
                $category['post'][$i]['post_id'] = $category['post_id'][$i];
            }
            unset($category['title']);
            unset($category['post_id']);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('home/index.html.twig', [
            'pagination' => $pagination,
            'posts' => $posts,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $query = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->findAllPostsInCategoryQuery($id);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        $category = $this
            ->getDoctrine()
            ->getRepository('App:Category')
            ->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Категория не найдена');
        }

        return $this->render('category/show.html.twig', [
            'pagination' => $pagination,
            'category' => $category
        ]);
    }
}
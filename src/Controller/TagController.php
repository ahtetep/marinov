<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $query = $this
            ->getDoctrine()
            ->getRepository('App:Tag')
            ->getAllTagQuery();
        ;

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('tag/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $query = $this
            ->getDoctrine()
            ->getRepository('App:Post')
            ->findAllPostsInTagQuery($id)
        ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            5
        );

        $tag = $this
            ->getDoctrine()
            ->getRepository('App:Tag')
            ->find($id)
        ;

        if (!$tag) {
            throw $this->createNotFoundException('Категория не найдена');
        }

        return $this->render('tag/show.html.twig', [
            'pagination' => $pagination,
            'tag' => $tag
        ]);
    }
}

$(document).ready(function(){

    // --------------------------- multidimensional menu ---------------------------

    $(".dropdown-menu > li > a.trigger").on("click",function(e){
        console.log(5);
        var current=$(this).next();
        var grandparent=$(this).parent().parent();
        if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
            $(this).toggleClass('right-caret left-caret');
        grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
        grandparent.find(".sub-menu:visible").not(current).hide();
        current.toggle();
        e.stopPropagation();
    });
    $(".dropdown-menu > li > a:not(.trigger)").on("click",function(){
        var root=$(this).closest('.dropdown');
        root.find('.left-caret').toggleClass('right-caret left-caret');
        root.find('.sub-menu:visible').hide();
    });

    // --------------------------- Slider ---------------------------
    $('.single-item').slick({
        autoplay: true,
        autoplaySpeed: 3000
    });
});